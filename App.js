import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, Button, TextInput, Modal, TouchableHighlight, Switch, CheckBox} from 'react-native';

export default function App() {
  const [modalVisible, setModalVisible] = useState(false);
  const [name, setName] = useState('');
  const [surname, setSurname] = useState('');
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);
  const [isSelected, setSelection] = useState(false);
  
  
  return (
    <View style={styles.container}>
      <Text style={{ ...styles.mainTitle }}>First Native</Text>
      <StatusBar style="auto" />
      <Text style={{ ...styles.titleText }}>Введите имя</Text>
      <TextInput style={styles.textInput} onChangeText={text => setName(text)} placeholder="введите имя" value={name}/>
      <Text style={{ ...styles.titleText }}>Введите фамилию</Text>
      <TextInput style={styles.textInput} placeholder="введите фамилию" onChangeText={text => setSurname(text)} value={surname}/>
      <Text style={{ ...styles.titleText }}>Получать рассылку?</Text>
      <Switch trackColor={{ false: '#d90d0d', true: '#00059c' }} thumbColor={isEnabled ? '#d90d0d' : '#00059c'}
        onValueChange={toggleSwitch} value={isEnabled}/>
      <Text style={{ ...styles.titleText }}>Довольны ли вы сервисом?</Text>
      <CheckBox value={isSelected} onValueChange={setSelection} style={styles.checkbox} />
      <Button onPress={() => setModalVisible(true)} title="Ввод"  />
      <Modal animationType="slide" transparent={true} visible={modalVisible}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Имя : {name}</Text>
            <Text style={styles.modalText}>Фамилия : {surname}</Text>
            <Text style={styles.modalText}>Получать рассылку? : {isEnabled ? "да" : "нет"}</Text>
            <Text style={styles.modalText}>Довольны ли вы сервисом? : {isSelected ? "👍" : "👎"}</Text>
            <TouchableHighlight
              style={{ ...styles.openButton }}
              onPress={() => { setModalVisible(!modalVisible); }}>
              <Text style={styles.textStyle}>Закрыть</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({

  mainTitle: {
    fontSize: 50,
    fontWeight: "bold",
    color:"red"
  },
  titleText: {
    color: "blue",
    fontSize: 20
  },
  textInput: {
    height: 30,
    width: 150,
    borderColor: 'red',
    borderWidth: 3
  },
  container: {
    flex: 1,
    backgroundColor: '#d1d1d1',
    alignItems: "center",
    justifyContent:"space-evenly"
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'yellow',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: 'red',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    color: "blue",
    textAlign: 'center',
  },
});
